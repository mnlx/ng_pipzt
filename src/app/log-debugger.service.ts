import {Injectable} from '@angular/core';

@Injectable()
export class LogDebuggerService {
	logger: object[] = [];

	add(message: any) {

			this.logger.push(message);

	}

	clear() {
		this.logger = [];
	}
}
