import {Component, Input, OnInit} from '@angular/core';
import {CronInterface, IntervalInterface, TasksInterface} from '../../Interfaces/VariousInterfaces';
import {SequenceInterface} from '../../Interfaces/VariousInterfaces';
import {LogDebuggerService} from '../../log-debugger.service';

@Component({
	selector: 'app-crontasks',
	templateUrl: './crontasks.component.html',
	styleUrls: ['../../static/bootstrap4/css/bootstrap.css']
})
export class CrontasksComponent implements OnInit {
	cronObject: CronInterface;
	intervalObject: IntervalInterface;
	tasksObject: TasksInterface;

	@Input() tasks: TasksInterface;

	constructor(public debug: LogDebuggerService) {
	}

	ngOnInit() {
	}

}
