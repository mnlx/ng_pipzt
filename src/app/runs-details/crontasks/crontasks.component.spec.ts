import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrontasksComponent } from './crontasks.component';

describe('CrontasksComponent', () => {
  let component: CrontasksComponent;
  let fixture: ComponentFixture<CrontasksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrontasksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrontasksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
