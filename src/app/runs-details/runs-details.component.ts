import {
	AfterContentChecked, AfterContentInit, AfterViewChecked, AfterViewInit, Component, OnChanges, OnDestroy,
	OnInit
} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ApiFetchService} from '../api-fetch.service';
import 'rxjs/add/operator/map';
import {SessionService} from '../session.service';
import {isNullOrUndefined, isUndefined} from 'util';
import {RunsInterface, SequenceInterface, TasksInterface} from '../Interfaces/VariousInterfaces';
import {_if} from 'rxjs/observable/if';
import {LogDebuggerService} from '../log-debugger.service';

@Component({
	selector: 'app-sequence-list',
	templateUrl: './runs-details.component.html',
	styleUrls: ['../static/bootstrap4/css/bootstrap.css']
})
export class RunsDetailsComponent implements OnInit, AfterContentChecked  {
	cronorinterval = 0; // If zero it means cron
	RUN_PK: number;
	classes = 'btn btn-block btn-danger';
	sequence_set: any[];


	sequenceClassSetter(obj: any, str: string): object {
		// appends Interfaces to sequence_item_copy in template

		return {
			alert: str === 'alert',
			'alert-info': obj.last_run === 0 && str === 'alert',
			'alert-success': obj.last_run === 1 && str === 'alert',
			'alert-danger': obj.last_run === 2 && str === 'alert',
			btn: str === 'btn',
			'btn-info': obj.last_run === 0 && str === 'btn',
			'btn-success': obj.last_run === 1 && str === 'btn',
			'btn-danger': obj.last_run === 2 && str === 'btn'
		};
	}

	addSequence(num: number, runs_object: RunsInterface) {
		const sequence_object: SequenceInterface = {
			pk: 0,
			name: 'sequence_' + num.toString(),
			fails_sum: 0,
			success_sum: 0,
			last_run: 0,
			owner: runs_object.pk,
			action_set: [
				{
					json: 'test',
					pk: 0,
					owner: 0,
				}
			]
		};
		if (num === 0 ) {

			runs_object.sequence_set = [sequence_object];
		}else {
			runs_object.sequence_set.splice(num, 0, sequence_object);
		}

	}

	removeSequence(num: number) {
		this.api.runs.sequence_set.splice(num, 1);
	}

	constructor(public route: Router,
			public log: LogDebuggerService,
			public api: ApiFetchService,
			public session: SessionService,
			public activeRoute: ActivatedRoute ) {
	}

	ngOnInit() {
		this.RUN_PK =  +this.activeRoute.snapshot.paramMap.get('pk');
		if (this.session.sessionIsActive()) {
			this.api.getRunsObservable(this.RUN_PK, this.session.TOKEN).subscribe(
				response => {
					this.api.runs = response;
				});
		} else {
			this.route.navigate(['/runs']);
		}
		console.log(1);


	}
	ngAfterContentChecked() {
		if (this.api.runs
			&& this.api.runs.celery_tasks
			&& !this.api.runs.celery_tasks.crontab
			&& this.api.runs.celery_tasks.interval) { // First it checks if runs exists then if interval is null
				this.cronorinterval = 1;
			}
	}
}
