import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {CronInterface, IntervalInterface, TasksInterface} from '../../Interfaces/VariousInterfaces';
import {LogDebuggerService} from '../../log-debugger.service';
import {isNull} from 'util';
import {ApiFetchService} from '../../api-fetch.service';
import {SessionService} from '../../session.service';

@Component({
	selector: 'app-intervaltasks',
	templateUrl: './intervaltasks.component.html',
	styleUrls: ['../../static/bootstrap4/css/bootstrap.css']
})
export class IntervaltasksComponent implements OnInit {
	// cronObject: CronInterface;
	// intervalObject: IntervalInterface;
	// tasksObject: TasksInterface;


	@Input() tasks: TasksInterface;

	active_button_classes: object;

	changeTaskStatus(): void {
		if (this.tasks.enabled === 'True') {
			this.tasks.enabled = 'False';

		} else {
			this.tasks.enabled = 'True';
		}
	}
	constructor(
		public api: ApiFetchService,
		public debug: LogDebuggerService,
		public session: SessionService

	) {
	}

	ngOnInit() {
		this.active_button_classes = {active: this.tasks.enabled === 'True',
			'btn-outline-primary': this.tasks.enabled === 'True',
			'btn-outline-light': this.tasks.enabled === 'False'};
	}



}
