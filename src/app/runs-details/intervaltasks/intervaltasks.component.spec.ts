import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntervaltasksComponent } from './intervaltasks.component';

describe('IntervaltasksComponent', () => {
  let component: IntervaltasksComponent;
  let fixture: ComponentFixture<IntervaltasksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntervaltasksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntervaltasksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
