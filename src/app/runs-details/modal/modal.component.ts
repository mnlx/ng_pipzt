import {Component, Input, OnInit} from '@angular/core';
import {RunsInterface} from '../../Interfaces/VariousInterfaces';
import {HttpClient} from '@angular/common/http';
import {ApiFetchService} from '../../api-fetch.service';
import {LogDebuggerService} from '../../log-debugger.service';
import index from '@angular/cli/lib/cli';
import {SequenceInterface} from '../../Interfaces/VariousInterfaces';


@Component({
	selector: 'app-modal',
	templateUrl: './modal.component.html',
	styleUrls: ['../../static/bootstrap4/css/bootstrap.css']
})


export class ModalComponent implements OnInit {

	@Input() sequence_item: SequenceInterface;
	@Input() sequence: RunsInterface;
	@Input() sequence_ind: number;

	sequence_item_copy: SequenceInterface;

	constructor(public http: HttpClient,
			public api: ApiFetchService,
			public debug: LogDebuggerService) {
	}


	saveCopySequence(sequence_item_copy: SequenceInterface, sequence_set_order: number) {
		this.api.runs.sequence_set[sequence_set_order] = sequence_item_copy;
		// this.sequence_item = JSON.parse(JSON.stringify(sequence_item_copy));
		console.log(this.sequence_item);
	}
	resetCopySequence(sequence_item: SequenceInterface) {
		this.sequence_item_copy = JSON.parse(JSON.stringify(sequence_item));
	}

	ngOnInit() {
		this.sequence_item_copy = JSON.parse(JSON.stringify(this.sequence_item)); // copying the object

	}

}
