import { Injectable } from '@angular/core';
import { isNullOrUndefined } from 'util';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UserInterface } from './Interfaces/VariousInterfaces';

@Injectable()
export class SessionService {
	USER: string;
	USER_OBJ: UserInterface;
	PASSWORD: string;
	TOKEN: string;
	TOKEN_OBJ: object;
	SESSION_URL = 'http://192.168.1.8:8000';

	constructor(
		private route: Router,
		private http: HttpClient) {
	}

	initiateSession(user: string, password: string) {
		this.USER = user;
		this.PASSWORD = password;
		const toSend = {
			username: user,
			password: password
		};
		const httpOptions = {
			headers: new HttpHeaders({ 'Content-Type': 'application/json' })
		};
		this.http.post(this.SESSION_URL + '/auth/token/create/',
			JSON.stringify(toSend),
			httpOptions)

			.subscribe(response => {
				this.TOKEN = response['auth_token'];
				this.TOKEN_OBJ = response;
				const httpOptions_with_token = {
					headers: new HttpHeaders({
						'Content-Type': 'application/json',
						'Authorization': `Token ${this.TOKEN}`
					})
				};
				this.http.get<UserInterface>(this.SESSION_URL + '/auth/me/',
					httpOptions_with_token).subscribe(data => {
					this.USER_OBJ = data;
					localStorage.setItem('user_session',
						JSON.stringify({
							token: response['auth_token'],
							is_active: true,
							user_obj: data
						}));
					console.log(this.USER_OBJ);
				});


			});
	}


	sessionIsActive(): boolean {
		// Checks local storage if session is already active, in case it is, it returns true ands adds the value to TOKEN and USER
		// Needs to integrate with token timeout time
		const currentUser: any = localStorage.getItem('user_session');
		if (!isNullOrUndefined(currentUser)) {
			const currentUserParsed: object = JSON.parse(currentUser);
			this.TOKEN = currentUserParsed['token'];
			this.USER_OBJ = currentUserParsed['user_obj'];
			return true; // is active
		} else {
			return false;
		}
	}

}


