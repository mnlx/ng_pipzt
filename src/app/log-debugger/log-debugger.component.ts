import {Component, OnInit} from '@angular/core';
import {LogDebuggerService} from '../log-debugger.service';
import {ApiFetchService} from '../api-fetch.service';
import {SessionService} from '../session.service';
import {HttpClient} from '@angular/common/http';
import {tap} from 'rxjs/operators';


@Component({
	selector: 'app-log-debugger',
	templateUrl: './log-debugger.component.html',
	styleUrls: ['./log-debugger.component.css']
})
export class LogDebuggerComponent implements OnInit {

	testObservables(): void {
		this.http.get(this.api.API_URL + '/runs/21').pipe(
			tap(data => {this.logger.add(data.toString()); })
		).pipe(
			tap(data => {this.logger.add(data.toString() + '_clone'); })
		);
	}

	constructor(
		public logger: LogDebuggerService,
		public api: ApiFetchService,
		public session: SessionService,
		// public runs: RunsDetailsComponent
		public http: HttpClient,
	) {
	}

	ngOnInit() {
		this.testObservables();
		// this.logger.add([this.runs.runs]);
	}

	// log(): void {
	// 	console.log(this.runs.runs);
	// }

}
