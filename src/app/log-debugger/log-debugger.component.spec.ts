import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {LogDebuggerComponent} from './log-debugger.component';

describe('LogDebuggerComponent', () => {
    let component: LogDebuggerComponent;
    let fixture: ComponentFixture<LogDebuggerComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [LogDebuggerComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(LogDebuggerComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
