import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {LogDebuggerService} from './log-debugger.service';
import {Observable} from 'rxjs/Observable';
import {RunsInterface, RunsListInterface, TasksInterface} from './Interfaces/VariousInterfaces';
// import {SessionService} from './session.service';
import {catchError, retry, tap, map} from 'rxjs/operators';
import {ErrorObservable} from 'rxjs/observable/ErrorObservable';
import {ActionInterface} from './Interfaces/VariousInterfaces';
import {error} from 'selenium-webdriver';
import {isUndefined} from 'util';
import { Router } from '@angular/router';



// import {exists} from 'fs';

@Injectable()
export class ApiFetchService {
	token: string;
	tokenObject: object;

	API_URL = 'http://192.168.1.8:8000';

	tokenObservable: Observable<any>;
	actionObservable: Observable<ActionInterface>;
	sequenceObservable: Observable<RunsInterface>;

	data: object;
	action: ActionInterface;
	runs: RunsInterface;



	constructor(private http: HttpClient,
	            private debug: LogDebuggerService,
	            private router: Router
	            // private session: SessionService
	) {
	}

	createSequence(obj: object): void {
		const httpOptions = {
			headers: new HttpHeaders({'Content-Type': 'application/json'})
		};
		this.http
			.post(
				this.API_URL + '/api/runs/list.json',
				JSON.stringify(obj),
				httpOptions)
			.subscribe(() => {
			});

	}

	createRun(user: number, token: string){
		const httpOptions = {
			headers: new HttpHeaders({'Content-Type': 'application/json',
									Authorization: 'Token '+ token})
		};
		this.http
			.post<RunsInterface>(
				this.API_URL + '/api/runs/list.json',
				JSON.stringify({"owner" : user
								}),
				httpOptions)
			.subscribe(data => { this.runs = data;
			 					this.router.navigate(['/runs/' + data['pk']])
			});
	}

	updateSequence( ind: number, sequence_set: object) {
		const httpOptions = {
			headers: new HttpHeaders({'Content-Type': 'application/json'})
		};

		this.http.put(
			this.API_URL + '/api/runs/' + ind,
			JSON.stringify(sequence_set),
			httpOptions).subscribe(() => {

		});

	}

	updateRun(runs_obj: RunsInterface, token: string){
		const httpOptions = {
			headers: new HttpHeaders({'Content-Type': 'application/json',
									Authorization: 'Token '+ token})
			};
		this.http.put(this.API_URL + `/api/runs/${runs_obj.pk}/` ,  JSON.stringify(runs_obj),httpOptions )
			.subscribe(data=>{console.log(data)});
	}


	updateTask(task_object: TasksInterface, token: string){
		const httpOptions = {
			headers: new HttpHeaders({'Content-Type': 'application/json',
				Authorization: 'Token '+ token})
		};
		this.http.put(this.API_URL + `/api/tasks/${task_object.pk}/` , JSON.stringify(task_object), httpOptions )
			.subscribe(data=>{console.log(data)});
	}


	getLastPK() {
		return this.http.get(this.API_URL + '/api/runs/last/');
	}

	getTokenObservable(user: string, password: string): Observable<any> {
		const toSend = {
			username: user,
			password: password
		};
		let httpOptions = {
			headers: new HttpHeaders({'Content-Type': 'application/json'})
		};
		return this.http.post(
			this.API_URL + '/auth/token/create/',
			JSON.stringify(toSend),
			httpOptions);
	}

	getRunsObservable(pk: number, token: string): Observable<RunsInterface> {
		let httpOptions =  {headers: new HttpHeaders({Authorization: 'Token ' + token })};
		return this.http.get<RunsInterface>(this.API_URL + '/api/runs/' + pk + '/',  httpOptions);
	}

	getRunsListObservablePage(token: string, page: string, user_id: number){
		const httpOptions =  {headers: new HttpHeaders({Authorization: 'Token ' + token })};
		if(isUndefined(page)) {
			return this.http.get<RunsListInterface>(this.API_URL + '/api/user/' + user_id +'/runs/list', httpOptions);
		}
		else{
			return this.http.get<RunsListInterface>(page, httpOptions);
		}
	}

	getPeriodTasksObservable(pk: number, ){}





	private handleError(error: HttpErrorResponse) {
		if (error.error instanceof ErrorEvent) {
			// A client-side or network error occurred. Handle it accordingly.
			console.error('An error occurred:', error.error.message);
		} else {
			// The backend returned an unsuccessful response code.
			// The response body may contain clues as to what went wrong,
			console.error(
				`Backend returned code ${error.status}, ` + `body was: ${error.error}`+
				`test: ${error.message}` + `${error.statusText}`)
		}
		// return an ErrorObservable with a user-facing error message
		return new ErrorObservable(
			'Something bad happened; please try again later.');
	}


}
