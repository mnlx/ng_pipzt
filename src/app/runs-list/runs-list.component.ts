import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';
import {ApiFetchService} from '../api-fetch.service';
import {SessionService} from '../session.service';
import {RunsInterface, RunsListInterface} from '../Interfaces/VariousInterfaces';


@Component({
	selector: 'app-sequence-list',
	templateUrl: './runs-list.component.html',
	styleUrls: ['../static/bootstrap4/css/bootstrap.css']
})
export class RunsListComponent implements OnInit {
	RunsListObject: RunsListInterface;

	constructor(private route: ActivatedRoute,
	            public  api: ApiFetchService,
	            public session: SessionService) {
	}

	ngOnInit(): void {
		this.api.getRunsListObservablePage(this.session.TOKEN, undefined, this.session.USER_OBJ.id).subscribe(data => {
			this.RunsListObject = data;
		});


	}

	changePage(page: string): void {
		const id = +this.route.snapshot.paramMap.get('pk');
		this.api.getRunsListObservablePage(this.session.TOKEN, page, id).subscribe(data => {
			this.RunsListObject = data;
		});
	}
}
