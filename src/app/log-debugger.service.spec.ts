import {inject, TestBed} from '@angular/core/testing';

import {LogDebuggerService} from './log-debugger.service';

describe('LogDebuggerService', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [LogDebuggerService]
        });
    });

    it('should be created', inject([LogDebuggerService], (service: LogDebuggerService) => {
        expect(service).toBeTruthy();
    }));
});
