import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {AppComponent} from './app.component';
import {RunsDetailsComponent} from './runs-details/runs-details.component';
import {RunsListComponent} from './runs-list/runs-list.component';
import {AppRoutingModule} from './app-routing.module';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {NavBarComponent} from './nav-bar/nav-bar.component';
import {ApiFetchService} from './api-fetch.service';
import {LogDebuggerComponent} from './log-debugger/log-debugger.component';
import {LogDebuggerService} from './log-debugger.service';
import {ModalComponent} from './runs-details/modal/modal.component';
import {LoginComponent} from './login/login.component';
import {SessionService} from './session.service';
import { CrontasksComponent } from './runs-details/crontasks/crontasks.component';
import {IntervaltasksComponent} from './runs-details/intervaltasks/intervaltasks.component';
// import {RunsDetailsComponent} from './runs-details/sequence.component';

@NgModule({
	declarations: [
		AppComponent,
		RunsDetailsComponent,
		RunsListComponent,
		NavBarComponent,
		LogDebuggerComponent,
		ModalComponent,
		LoginComponent,
		CrontasksComponent,
		IntervaltasksComponent

	],
	imports: [
		BrowserModule,
		FormsModule,
		AppRoutingModule,
		HttpClientModule,

		// TasksRoutingModule,


	],
	providers: [ApiFetchService, LogDebuggerService, SessionService],
	bootstrap: [AppComponent,
		NavBarComponent,
		LogDebuggerComponent]
})
export class AppModule {
}
