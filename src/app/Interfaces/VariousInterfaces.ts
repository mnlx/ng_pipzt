export interface IntervalInterface {
	every: number;
	period: string;
}

export interface CronInterface {
	minute?: string;
	hour?: string;
	day_of_week?: string;
	day_of_month?: string;
	month_of_year?: string;
}
export interface TasksInterface {
	pk: number;
	name: string;
	crontab?: CronInterface;
	interval?: IntervalInterface;
	args?: string[];
	kwargs?: object;
	expires?: string;
	enabled: string;
	task: string;
}

export interface RunsInterface {
	pk: number;
	create_date: string;
	celery_tasks?: TasksInterface;
	sequence_set: SequenceInterface[];
}

export interface SequenceInterface {
	pk: number;
	owner: number;
	name: string;
	fails_sum: number;
	success_sum: number;
	last_run: number;
	action_set: ActionInterface[];
}

export interface ActionInterface {
	pk: number;
	owner: number;
	json: string;
}


export interface RunsListInterface {
	results: RunsInterface[];
	next: string;
	previous: string;
}

export interface UserInterface {
	email?: string;
	id: number;
	username: string;
}
