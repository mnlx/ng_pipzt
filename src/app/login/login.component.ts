import {ApiFetchService} from '../api-fetch.service';
import {Router} from '@angular/router';
import {Component, DoCheck, OnDestroy, OnInit} from '@angular/core';
import {SessionService} from '../session.service';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {RunsInterface} from '../Interfaces/VariousInterfaces';
import {isNullOrUndefined, isUndefined} from 'util';
import {logger} from 'codelyzer/util/logger';
import {LogDebuggerService} from '../log-debugger.service';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnDestroy, DoCheck, OnInit {

	constructor(private api: ApiFetchService,
			public route: Router,
			private http: HttpClient,
			private debug: LogDebuggerService,

	public session: SessionService,
	) {}


	ngOnDestroy() {

		// this.api.sequenceObservable = this.api.getRunsObservable(21, 'Token ' + this.session.TOKEN);

	}
	ngDoCheck() {
		this.debug.add(this.session.sessionIsActive()['is_active']);
	}

	ngOnInit() {
		// this.debug.add(isNullOrUndefined(this.session.TOKEN));
		// this.debug.add('test');
		// if ( !isNullOrUndefined( this.session.TOKEN )) {
		// 	this.route.navigate(['/runs']);
		// }
	}
}
