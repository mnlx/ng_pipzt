import {Component} from '@angular/core';
import {ApiFetchService} from './api-fetch.service';
import {Router} from '@angular/router';
import {SessionService} from './session.service';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./static/bootstrap4/css/bootstrap.css'],

})
export class AppComponent {
	title = 'PIPZ.t';

	constructor(public session: SessionService) {
	}

}
