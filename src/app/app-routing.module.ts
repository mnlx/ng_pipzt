import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {RunsDetailsComponent} from './runs-details/runs-details.component';
import {RunsListComponent} from './runs-list/runs-list.component';
import {LoginComponent} from './login/login.component';
// import {CeleryTasksComponent} from './sequence-results-list/IntervalTasks/celery-tasks.component';


const routes: Routes = [
	// {path: 'detail/:id', component: RunsListComponent},
	{path: 'runs', component: RunsDetailsComponent},
	{path: 'user/:pk/runs/list', component: RunsListComponent},
	{path: 'runs/:pk', component: RunsDetailsComponent},


	// {path: '', component: RunsDetailsComponent},
	// {path: '**', component: RunsDetailsComponent},
	// {path: 'celery', component: CeleryTasksComponent},
	// {path: 'login', component: LoginComponent}
];

@NgModule({
	exports: [RouterModule],
	imports: [RouterModule.forRoot(routes)]
})
export class AppRoutingModule {
}
