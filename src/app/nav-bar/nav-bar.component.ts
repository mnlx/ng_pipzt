import {Component, OnInit} from '@angular/core';
import { ApiFetchService } from '../api-fetch.service';
import { SessionService } from '../session.service';
import { AppRoutingModule } from '../app-routing.module';
import { Router } from '@angular/router';

@Component({
    selector: 'app-nav-bar',
    templateUrl: './nav-bar.component.html',
    styleUrls: ['../static/bootstrap4/css/bootstrap.css'],
})
export class NavBarComponent implements OnInit {

    constructor( public api: ApiFetchService,
		     public session: SessionService,
		     public router: Router) {
    }

    ngOnInit() {
    }

}
