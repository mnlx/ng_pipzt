#PIPZ_bot + Site

The objective of this program is to test the PIPZ site and send the information obtained to a site.

Using:
chrome-drive==2.34
postgresql==9.5

#Divisão do program

O programa se divide em duas principais áreas:

    1 - O robô em si que usa selenium
    2 - O site que mostra o resultado dos testes feitos pelo robô (AngularJS e Django)


##1 - O ROBÔ

O robo divide suas operações em sequências, e as sequências se dividem am ações. Essa sequências estão salvas na base de
dados na tabela sequence. Cada row é contém uma ação e existem as colunas da primarykey, número da sequência e a ação.
Essas ações estarão estocadas em formato JSON e será transformado em tuple dentro do programa (espera-se).

As sequências serão removidas ou adicionadas pelo site, que conterá uma aba especifica para a criação de sequências. As
ações que estão dentro das sequencias se dividem em find, click, send_keys e login (serão acrescentado mais futuramente).
Essas, por enquanto, são as 4 ações básicas necessárias e que se repetiram várias vezes durante o teste.

Por rodar em um servidor e para postar no site também, serão tiradas 2 screenshots para cada ação, uma antes e outra
depois da ação se completar.


Resumindo portanto a operacionalização do robo:

    1 Primeiro ele lê a tabela sequence
    2 Depois ele começa a rodar as ações dentro de cada sequências
    3 Ele então pode obter o resultado succesful ou failed de cada ação
    4 Finalmente serão estocados na tabela runs as ações, seus resultados e suas imagems.

##2 - O SITE

O site tem as seguintes abas : sequências, resultados e status.

A parte do FrontEnd será feito por AngularJS que comunicará com o backend feito principalmente em Django.

Como mencionado anteriormente, as sequencias são modificadas apenas pelo site, portanto essa aba contém as sequências
estocadas na base de dados. Ao usuário será dado o direito de remover ou adicionar sequencias.

Resultados. Essa aba contem os resultados das sequencias depois de cada vez que o robo roda. A curto prazo o objetivo é
apenas ter tabelas. A longo prazo, essa aba também terá gráficos.

Status, mostra o status do robô. Mas também permite modificar certa configurações do robo como por exemplo se ele está
ativo ou não, e o tempo de timeout entre cada teste.

###FrontEnd:

Desenvoldido em `AngularJS`.  


- Usar o seguinte URL para AJAX http://127.0.0.1:8000/api/runs/50/



##3 - BASE DE DADOS


Contém três tabelas: 1 runs(seguirá a estrutura do modelos do django), 2 sequence(similar a runs na estrutura do modelo mas não tera  a parte de resultados) e 3 status().

- 1 Runs:

as colunas: id	run_number	sequence_name	action_type	action_aux1	action_aux2	result	image1	image2


Selenium server setup:

 -[x] Setup virtualenv for Python in ~/seleniumvenv/
 -[x] use command xvfb-run --auto-servernum --server-args="-screen 0 1600x900x8" python3 [insert selenium command]
